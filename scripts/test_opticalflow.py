# -*- coding: utf-8 -*-

import os
from natsort import natsorted
import time

import numpy as np
import cv2
import pyflow
import matplotlib.pyplot as plt
import seaborn as sns

import kinectmatics as km
from kinectmatics import libvid

#%% Load FreeBe data and convert to normal data format (folder of images)
dir_data = 'E:/NY591'
dir_save = 'D:/data/workspace_opticalflow/test_process_resize2_full'

if not os.path.exists(dir_save):
    os.makedirs(dir_save)
bool_visualize = True
bool_savedata = True
res=(1920,1080)

def _scale_res(res, scale=4):
    return (int(res[0]/scale), int(res[1]/scale))

res = _scale_res(res)

flist = [f for f in os.listdir(dir_data) if os.path.isdir(os.path.join(dir_data, f)) and not f=='Support Data' and not f=='extracted data' and not f=='figures' ]
flist = natsorted(flist)
flist = flist[7:]

#%% generate optical flow

for f in flist:
    image_list, image_path = km.get_image_list(dir_data, f, imtype='Color', image_fmt='jpg') 
    
#    image_list = image_list[:120]
    # NEED TO COLLECT TIME, research purposes

#    libvid.frames_to_video(image_path, 'D:/data/workspace_opticalflow/test_flow_par/raw.mp4', fs=15.0, res=res, keep_frames=True)

    km.process_flow_from_list(image_list, dir_save=dir_save, dat=bool_visualize, viz=bool_savedata, res=res, n_jobs=-1)
#    break
    np.save('{0}/image_list'.format(dir_save), image_list[:-1])
    break
#%
#% process optical flow for total movement and artifact detection (via boundaries)
    
def make_border_mask(res, buff_h=0.1, buff_v=0.1):
    mask = np.zeros(res)
    
    width = int(res[0]*buff_h)
    height = int(res[1]*buff_v)
    
    mask[:width,:] = 1
    mask[-width:,:] = 1
    mask[:,:height] = 1
    mask[:,-height:] = 1
    
    return mask.astype(bool)
    
    
    #%

#dev
from kinectmatics import opticalflow as kof

artifact_mask = make_border_mask(res, buff_h=0.1, buff_v=0.1).T

for f in flist:
    dir_flow = os.path.join(dir_save, 'flow_data')
    image_list = natsorted([os.path.join(dir_flow, f) for f in os.listdir(dir_flow) if os.path.isfile(os.path.join(dir_flow, f)) and f[-3:] =='png'])
    image_list = np.reshape(image_list, (2,-1)).T
    
    magnitude = np.zeros((len(image_list),))
    border = np.zeros((len(image_list),))
        
    for i, (fx, fy) in enumerate(image_list):
        mag, ang = kof.load_flow(fx, fy)
        
        magnitude[i] = mag.sum()
        border[i] = mag[artifact_mask].sum()
        
        
#        break\
    
    np.save(os.path.join(dir_save, 'flow_magnitude'), magnitude)
    np.save(os.path.join(dir_save, 'flow_border'), border)
    
#    break
    
    #%
    from scipy import stats
    magnitude_z = stats.zscore(magnitude)
    border_z = stats.zscore(border)

    t = np.linspace(0, len(magnitude)/15. / 60, len(magnitude))
    
    fig, ax = plt.subplots(1)
#    plt.plot(t, magnitude - magnitude.mean())
    ax.plot(t, magnitude)
    ax.plot(t, border)
    ax.plot(t, magnitude-border)
    plt.legend(('full mag', 'border', 'diff'))
    plt.xlim((t[0], t[-1]))
    fig.savefig(os.path.join(dir_save, 'flow_magnitude.png'))

    #%
    
    break
