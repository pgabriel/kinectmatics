# -*- coding: utf-8 -*-

import numpy as np

import kinectmatics as km

# %% Test threshold
signal = np.linspace(0, 1)

signal_threshold = km.threshold_signal(signal, thresh=0.5)

print(signal_threshold)

# %% Test isolation filter
signal = np.array([1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1])

filtered_signal = km.merge_neighboring_cells(signal)
filtered_signal = km.filter_isolated_cells(
    filtered_signal, struct=None, len_isolation=1)

#% Test array to sequence

df_sequence = km.array_to_sequence(filtered_signal, invert=False)

print(filtered_signal)
print(df_sequence)

#%%

print(filtered_signal)