# -*- coding: utf-8 -*-
"""
Created on Tue Jul 24 19:57:42 2018

@author: kali
"""

# -*- coding: utf-8 -*-

import os
from natsort import natsorted
import time

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

import kinectmatics as km
    
    
#%%
def extract_range(signal, time=None):
    if time is None:
        time=np.arange(len(signal))
    ranges = km.array_to_sequence(signal)
    onsets = time[ranges['onset'].values]
    offsets = time[ranges['onset'].values+ranges['duration'].values - 1]
    durations = offsets - onsets
    
    ranges['onset'] = onsets
    ranges['duration'] = durations
    ranges['offset'] = offsets
    
    return ranges
    
    
#%
def extract_labels_flow(df, thresh_joint=0.5):
    # basic movement ranges
    b_left = False
    b_right = False
    b_joint = False
    b_rest = False
    b_still = False 
    
    ranges = []
    try:
        ranges_left = extract_range(df['b_left'].values, df['kT'].values)
        ranges_left['label'] = 'Left'
        b_left = True
    except:
        pass
    
    try:
        ranges_right = extract_range(df['b_right'].values, df['kT'].values)
        ranges_right['label'] = 'Right'
        b_right = True

    except:
        pass
    
    try:
        s_still = df['@Movement'].values == 1
        ranges_still = extract_range(s_still, df['kT'].values)
        ranges_still['label'] = 'Still'
        b_still = True
    except:
        pass
    
    try:    
        s_rest = df['@Movement'].values == 0
        ranges_rest = extract_range(s_rest, df['kT'].values)
        ranges_rest['label'] = 'Rest'
        b_rest = True
    except:
        pass
#    ## use head as additional rejection criteria for left and right ranges
#    ranges_head = extract_range(df['b_head'].values, df['kT'].values)
#    ranges_head['label'] = 'Head'
    
    try:
        # find ranges where left and right onsets overlap
        ranges_joint = []
        ranges_left_only = []
        ranges_right_only = []
        for _, rngl in ranges_left.iterrows():
            onset = rngl['onset']
            diffs = onset - ranges_right['onset'].values
            i = np.argmin(np.abs(diffs))
            
            rngr = ranges_right.iloc[i]
            
            # rngl is ahead of 
            if np.abs(diffs[i]) <= thresh_joint:
                rng = rngl.copy()
                rng['duration'] = -1
                rng['onset'] = onset+diffs[i]
                rng['offset'] = -1
                rng['label'] = 'Joint'
                ranges_joint.append(rng)
            else:
                ranges_left_only.append(rngl)
        
        # now repeat for right movements, but don't bother updating ranges_joints
        for _, rngr in ranges_right.iterrows():
            onset = rngr['onset']
            diffs = onset - ranges_left['onset'].values
            i = np.argmin(np.abs(diffs))
            
            rngl = ranges_left.iloc[i]
            
            # rngl is ahead of 
            if np.abs(diffs[i]) >= thresh_joint:
                ranges_right_only.append(rngr)
                
        ranges_joint = pd.concat(ranges_joint, ignore_index=True, axis=1).T
        ranges_left = pd.concat(ranges_left_only, ignore_index=True, axis=1).T
        ranges_right = pd.concat(ranges_right_only, ignore_index=True, axis=1).T
        b_joint = True

    except:
        pass
    
    if b_left: ranges.append(ranges_left) 
    if b_right: ranges.append(ranges_right) 
    if b_joint: ranges.append(ranges_joint) 
    if b_still: ranges.append(ranges_still) 
    if b_rest: ranges.append(ranges_rest) 
        
    if len(ranges) > 0:
        ranges = pd.concat(ranges, ignore_index=True)
        ranges.sort_values(['onset'], inplace=True)
        ranges.reset_index(inplace=True, drop=True)
    else:
        ranges = None
        
    return ranges

def mark_movement(df, signal, t=None, buff=1):
    if t is None:
        t=np.arange(len(signal))
        
    detected = np.zeros(len(df))
    
    for i, event in df.iterrows():
        i_start = np.argmin(np.abs(event['onset'] - buff - t))
        i_end = np.argmin(np.abs(event['onset'] + buff - t))
        x = signal[i_start:i_end]
        detected[i] = np.sum(x) if len(x) > 0 else 0
    return detected

#%
# LOAD files
filename_manual = 'D:\\data\\workspace_onset\\manual_annotations_201509061823.dat'
filename_flow = 'D:\\data\\workspace_onset\\motion_report-201509061823.csv'

df_movement = pd.read_csv(filename_flow)
df_onsets_manual = pd.read_pickle(filename_manual)

# Clean up manual annotations
labels_manual = df_onsets_manual['label'].values
df_onsets_manual['label_ng'] = labels_manual
labels_manual[labels_manual == b'l'] = 'Left'
labels_manual[labels_manual == b'r'] = 'Right'
df_onsets_manual['label'] = labels_manual
df_onsets_manual['duration'] = df_onsets_manual['offset'].values - df_onsets_manual['onset'].values


# given df_movement:
df_onsets_flow = extract_labels_flow(df_movement)

# label onsets with head movement (binary, avg magnitude)
detected_head = mark_movement(df_onsets_flow, df_movement['b_head'].values, df_movement['kT'].values)

s_head = df_movement['mag_head'].values
s_head[np.logical_not(df_movement['b_head'].values)] = 0
mag_head = mark_movement(df_onsets_flow, s_head, df_movement['kT'].values)

df_onsets_flow['@Head'] = detected_head.astype(bool)
df_onsets_flow['@HeadMag'] = mag_head
