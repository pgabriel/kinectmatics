# -*- coding: utf-8 -*-

import os

import numpy as np
import cv2
import pyflow
import matplotlib.pyplot as plt

import kinectmatics as km
#%%
file_video = 'scripts/dat/neonate.avi'
dir_save = 'scripts/dat/'

km.video_to_frames(file_video, dir_save)

#%%
dir_frames = 'scripts/dat/neonate'
file_video = 'scripts/dat/neonate2.mp4'

km.frames_to_video(dir_frames, file_video, fs=30.0, res=(320,240))

#%%
dir_frames = 'scripts/dat/neonate'
dir_save = 'scripts/dat/'
viz = False
dat = True
res=(320,240)

flist = km.folder_to_list(dir_frames)
#%%
plt.imshow(im_prev)